import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None
from tqdm import tqdm

import os
import zipfile
from joblib import delayed, Parallel
from itertools import product
from utils import CHARAS_LIST

import warnings
warnings.filterwarnings('ignore')

if os.path.exists("data_patent"):
    print("this path")
    mon_ret=pd.read_pickle("data_patent/month_ret.pkl")
    mon_ret=mon_ret.fillna(0)
    datashare=pd.read_pickle("data_patent/datashare.pkl")
    # datashare=datashare[datashare["permno"]=="001872.XSHE"]
    # mon_ret=mon_ret[mon_ret["date"]>20210101]
    # datashare=datashare[datashare["date"]>20210101]
    # datashare=datashare.fillna(0)
    datashare["permno"]=datashare["permno"].apply(lambda x: int(x.split("/")[-1].split('.')[0]))

    mon_ret["permno"]=mon_ret["permno"].apply(lambda x: int(x.split("/")[-1].split('.')[0]))

else:

    dir_path="/data4022/lihao/filelist"

    ret = pd.read_pickle(os.path.join(dir_path,'stock_returns.pk'))
    retw = np.log(1 + ret.fillna(0.)).resample('W-Sun').sum()
    retwnew=retw.reset_index()
    retres=retwnew.melt(id_vars=["date"],value_vars=retwnew.columns[1:])
    mon_ret=retres.rename(columns={"symbol":"permno","date":"date","value":"ret-rf"})
    mon_ret["date_tmp"]=mon_ret["date"]
    mon_ret["date"]=mon_ret["date_tmp"].dt.strftime('%Y%m%d')
    mon_ret["date"]=mon_ret["date"].apply(int)
    del mon_ret["date_tmp"]
    mon_ret["ret-rf"]=mon_ret["ret-rf"].apply(float)*100

    datashare=pd.read_pickle("beta.pkl")
    datashare=datashare.reset_index()
    datashare=datashare.rename(columns={"ticker":"permno","reportdate":"date"})
    datashare["date"]=datashare["date"].apply(lambda x: int("".join(x.split("-"))))
    # datashare["date"]=datashare["date"].apply(int)

    datashare=pd.merge(datashare,mon_ret,on=["permno","date"],how="left")

# exit()




def pre_process(date):
    cross_slice = datashare.loc[datashare.date == date].copy(deep=False)
    try:
        omitted_mask = 1.0 * np.isnan(cross_slice.loc[cross_slice['date'] == date])
    except Exception as e:
        print(cross_slice)
        print(date)
        import pickle
        pickle.dump(cross_slice,open("debug.pkl","wb"))
        print(str(e))
        exit()
    # fill nan values with each factors median
    cross_slice.loc[cross_slice.date == date] = cross_slice.fillna(0) + omitted_mask * cross_slice.median()
    # if all stocks' factor is nan, fill by zero
    cross_slice.loc[cross_slice.date == date] = cross_slice.fillna(0)

    re_df = []
    # rank normalization
    for col in CHARAS_LIST:
        series = cross_slice[col]
        de_duplicate_slice = pd.DataFrame(series.drop_duplicates().to_list(), columns=['chara'])
        series = pd.DataFrame(series.to_list(), columns=['chara'])
        # sort and assign rank, the same value should have the same rank
        de_duplicate_slice['sort_rank'] = de_duplicate_slice['chara'].argsort().argsort()
        rank = pd.merge(series, de_duplicate_slice, left_on='chara', right_on='chara', how='right')['sort_rank']
        # if all values are zero, the results will contain nan
        rank_normal = ((rank - rank.min())/(rank.max() - rank.min())*2 - 1)
        re_df.append(rank_normal)
    re_df = pd.DataFrame(re_df, index=CHARAS_LIST).T.fillna(0)
    re_df['permno'] = list(cross_slice['permno'].astype(int))
    re_df['date'] = list(cross_slice['date'].astype(int))

    return re_df[['permno', 'date'] + CHARAS_LIST]



def cal_portfolio_ret(it, df):
    d, f = it[0], it[1]
    # long portfolio, qunatile 0.0~0.1; short portfolio, qunatile 0.9~1.0
    long_portfolio = df.loc[df.date == d][['permno', f]].sort_values(by=f, ascending=False)[:df.loc[df.date == d].shape[0]//10]['permno'].to_list()
    short_portfolio = df.loc[df.date == d][['permno', f]].sort_values(by=f, ascending=False)[-df.loc[df.date == d].shape[0]//10:]['permno'].to_list()
    # long-short portfolio return
    long_ret = mon_ret.loc[mon_ret.date == d].drop_duplicates('permno').set_index('permno').reindex(long_portfolio)['ret-rf'].dropna().mean()
    short_ret = mon_ret.loc[mon_ret.date == d].drop_duplicates('permno').set_index('permno').reindex(short_portfolio)['ret-rf'].dropna().mean()
    chara_ret = 0.5*(long_ret - short_ret)
    
    return chara_ret


def cal_portfolio_charas(month, df):
    mon_portfolio_chara = []
    p_name = [f'p_{chr}' for chr in CHARAS_LIST]
    for chr in CHARAS_LIST:
        # print(df.loc[df.date == month])
        long_portfolio = df.loc[df.date == month].sort_values(by=chr, ascending=False).reset_index(drop=True)[:df.loc[df.date == month].shape[0]//10]['permno'].to_list()
        short_portfolio = df.loc[df.date == month].sort_values(by=chr, ascending=False).reset_index(drop=True)[-df.loc[df.date == month].shape[0]//10:]['permno'].to_list()
        
        long_charas = df.loc[df.date == month].set_index('permno').loc[long_portfolio][CHARAS_LIST]
        short_charas = df.loc[df.date == month].set_index('permno').loc[short_portfolio][CHARAS_LIST]
        # print(f"long_charas is {long_charas},short_charas is {short_charas}")

        mon_portfolio_chara.append([month] + (0.5*(long_charas.mean() - short_charas.mean())).to_list())
        # exit()
    return pd.DataFrame(mon_portfolio_chara, index=p_name, columns=['date']+CHARAS_LIST)



if __name__ == '__main__':
    # pre-process share data

    processed_df = Parallel(n_jobs=-1)(delayed(pre_process)(d) for d in tqdm(datashare.date.drop_duplicates().to_list(), colour='green', desc='Processing'))
    processed_df = pd.concat(processed_df)

    ##TODO: calculate portfolio returns (or download preprocessed data)
    iter_list = list(product(datashare.date.drop_duplicates(), CHARAS_LIST))
    # print(iter_list)
    # for it in iter_list:
    #     print(it)
    #     cal_portfolio_ret(it, df=processed_df)
    # exit()

    portfolio_rets = Parallel(n_jobs=-1)(delayed(cal_portfolio_ret)(it, df=processed_df) for it in tqdm(iter_list, colour='green', desc='Calculating'))
    portfolio_rets = pd.DataFrame(np.array(portfolio_rets).reshape(-1, len(CHARAS_LIST)), index=datashare.date.drop_duplicates(), columns=CHARAS_LIST).reset_index()
    portfolio_rets[CHARAS_LIST] = portfolio_rets[CHARAS_LIST].astype(np.float16)
    
    
    ##TODO: calculate portfolio characteristics (or download preprocessed data)
    mon_list = pd.read_pickle('data_patent/mon_list.pkl')
    # print(f"process df is {processed_df}")
    # tmp=cal_portfolio_charas(20231030,processed_df)
    # exit()

    # for mon in tqdm(mon_list, colour='yellow', desc='Calculating P characteristics'):
    #     cal_portfolio_charas

    _portfolio_chara_set = Parallel(n_jobs=1)(delayed(cal_portfolio_charas)(mon, df=processed_df) for mon in tqdm(mon_list, colour='yellow', desc='Calculating P characteristics'))
    p_charas = _portfolio_chara_set[0].copy(deep=False)
    for tdf in _portfolio_chara_set[1:]:
        p_charas = pd.concat([p_charas, tdf])
    # print(p_charas)
            
    processed_df.to_pickle('data_patent/datashare_re.pkl')
    portfolio_rets.to_pickle('data_patent/portfolio_ret.pkl')
    p_charas.to_pickle('data_patent/p_charas.pkl')

    # processed_df.to_pickle('data_patent/datashare_re_sample.pkl')
    # portfolio_rets.to_pickle('data_patent/portfolio_ret_sample.pkl')
    # p_charas.to_pickle('data_patent/p_charas_sample.pkl')