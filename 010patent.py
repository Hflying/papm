import numpy as np
import pandas as pd
import ast
from datetime import datetime
from sklearn.linear_model import LinearRegression
import pickle
import os
# convert patent statistic to dataframe
# patent = pd.read_excel('D:/02 python projects\project\data\patent/topics_statistic.xlsx')
# s = patent['data']
# df = pd.DataFrame(0, index=range(len(patent)), columns=range(500))
#
# for i, item in enumerate(s):
#     try:
#         dic = ast.literal_eval(item)  # 将字符串转换为字典
#         for key, value in dic.items():
#             if key in df.columns:
#                 df.at[i, key] = value
#     except ValueError as e:
#         print(f"解析错误在索引 {i}，值为：{item}")
#         print("错误信息：", e)
#
#
# df.index = patent['report_date']
# df = df.sort_index()

dir_path="/data4022/lihao/filelist"

df = pd.read_pickle(os.path.join(dir_path,'topic_df.pk'))
df = df[datetime(2010,1,1):]

df_scaled = df.div(df.sum(axis=1), axis=0)

ret = pd.read_pickle(os.path.join(dir_path,'stock_returns.pk'))
retw = np.log(1 + ret.fillna(0.)).resample('W-Sun').sum()


data = pd.read_csv(os.path.join(dir_path,"bge_a_company_weekly_statistic","bge_a_company_weekly_statistic.csv"))
data.set_index(['reportdate','ticker'], inplace=True)
data = data['vector_t3y']
levels = data.index.levels[1].tolist()
modified_levels = data.index.levels[1].str.replace('.SH', '.XSHG').str.replace('.SZ', '.XSHE')
new_index = data.index.set_levels(modified_levels, level=1)
data.index = new_index


beta = pd.DataFrame(0., index=data.index, columns=range(500))

for index, value in data.items():
    dt = index[0]
    stk = index[1]
    if "BJ" in stk:
        continue
    print(dt, stk)
    v_dict = eval(value)
    v_dict = {k: v for k, v in v_dict.items() if v != 0}
    if len(v_dict) == 0:
        continue
    pt_topics = list(v_dict.keys())
    # prepare x and y
    if stk in retw.columns and set(pt_topics) <= set(df_scaled.columns):
        y = retw[stk][:dt][-52*3:]
        x = df_scaled[pt_topics][:dt][-52*3:]
        x, y = x.align(y, join='inner', axis=0)
        model = LinearRegression()
        try:
            model.fit(x, y)
            beta.loc[(dt, stk)].loc[pt_topics] = model.coef_
        except Exception as e:
            print(str(e))
            print(x)
            print(y)


pickle.dump(beta,open("beta.pkl","wb"))